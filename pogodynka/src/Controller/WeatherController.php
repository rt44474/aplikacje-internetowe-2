<?php // src/Controller/WeatherController.php
namespace App\Controller;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use App\Entity\Location;
use App\Repository\MeasurementRepository;
use App\Repository\LocationRepository;
class WeatherController extends AbstractController
{
    public function cityAction(string $country,string $city, MeasurementRepository $measurementRepository, LocationRepository $locationRepository): Response
    {
        $location = $locationRepository->findOneBy(['name' => $city,'country' => $country]);
        $measurements = $measurementRepository->findByLocation($location);
        return $this->render('weather/city.html.twig', [
            'city' => $city,
            'country' => $country,
            'measurements' => $measurements,
        ]);
    }
}